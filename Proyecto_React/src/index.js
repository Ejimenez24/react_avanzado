import React from 'react';
import ReactDOM from 'react-dom'; // Librería react-dom
import { HashRouter as Router, Route, Switch } from 'react-router-dom'; // Librería react-router-dom
import './index.css';

import * as reportWebVitals from '../reportWebVitals';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';

import Home from './components/home/Home';
import Nosotros from './components/nosotros/Nosotros';
import Servicios from './components/servicios/Servicios';
import Contacto from './components/contacto/Contacto';

ReactDOM.render(
    <Router>
	    <div>
	    	<Switch>
		        <Route exact path='/' component={Home} />
		        <Route path='/Nosotros' component={Nosotros} />
		        <Route path='/Servicios' component={Servicios} />
		        <Route path='/Contacto' component={Contacto} />

	      	</Switch>
	    </div>
    </Router>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA