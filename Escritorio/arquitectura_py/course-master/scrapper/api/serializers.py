from rest_framework import serializers
from scrapper.models import Movie

class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('movie_name','movie_year','movie_rating')