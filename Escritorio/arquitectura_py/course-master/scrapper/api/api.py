from response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from scrapper.api.serializers import MovieSerializer
from scrapper.models import Movie

@api_view(['GET'])
def movie_api_view(request):

    if request.method == 'GET':
        movie = Movie.objects.all()
        movie_serializer = MovieSerializer(movie, many=True)
        return Response(movie_serializer.data)