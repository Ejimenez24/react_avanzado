from django.urls import path
from scrapper.api.api import movie_api_view

urlpatterns = [
    path('movie/', movie_api_view, name='movie_api_view')
]