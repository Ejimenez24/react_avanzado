# Generated by Django 3.1.7 on 2021-05-08 04:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrapper', '0003_auto_20210506_0519'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
    ]
