from django.contrib import admin
from scrapper.models import Movie

@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ('movie_name', 'movie_year', 'movie_rating')
    search_fields = ('movie_name', 'movie_year')