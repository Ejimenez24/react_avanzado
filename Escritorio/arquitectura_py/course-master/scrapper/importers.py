from django.core.exceptions import ValidationError

from data_importer.core import DataImporter
from scrapper.models import Movie


class MovieImporter(DataImporter):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.columns = (
            ('movie_name', 0),
            ('movie_year', 1),
            ('movie_rating', 2),
        )

    def clean_movie_name(self, value):
        if not value:
            raise ValidationError('Movie is required.')
        return value

    def save(self, cleaned_data):
        instance = Movie(
            name=cleaned_data['movie_name']
        )

        if cleaned_data['movie_year']:
            instance.movie_year = cleaned_data['movie_year']

        if cleaned_data['movie_rating']:
            instance.movie_rating = cleaned_data['movie_rating']

        instance.save()
