from django.shortcuts import render

from django.shortcuts import render
from django.urls import reverse_lazy

from data_importer.forms import FileUploadHistoryForm
from scrapper.importers import MovieImporter
from scrapper.models import Movie
from scrapper.forms import MovieForm
from django.views.generic import ListView, DetailView, CreateView


# Create your views here.


def list_movie(request):
    movies = Movie.objects.all().values('id','movie_name')

    context = {
        'movies': movies
    }
    return render(request, 'p_test/list.html', context)


class MovieListView(ListView):
    template_name = 'p_test/list.html'
    model = Movie
    context_object_name = 'movies'

    def get_queryset(self):
        movies = Movie.objects.all()
        return movies


def detail_movie(request, id):
    try:
        movies = Movie.objects.get(id=id)
        exists = True
    except Movie.DoesNotExist:
        movies = 'Does not exist.'
        exists = False

    context = {
        'movies': movies,
        'exists': exists
    }

    return render(request, 'p_test/detail.html', context)


class MovieDetailView(DetailView):
    template_name = 'p_test/detail.html'
    queryset = Movie.objects.all()


def update_person(request, id, movie_name):
    try:
        movies = Movie.objects.get(id=id)
        Movie.movie_name = movie_name
        movies.save()
        message = 'Updated.'
    except Movie.DoesNotExist:
        message = 'Not Updated.'

    context = {
        'message': message
    }

    return render(request, 'p_test/update.html', context)


def create_movie(request):
    title = 'Create Movie'

    if request.method == 'POST':
        form = MovieForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    else:
        form = MovieForm()

    context = {
        'form': form,
        'title': title
    }

    return render(request, 'p_test/create.html', context)


class MovieCreateView(CreateView):
    template_name = 'p_test/create.html'
    form_class = MovieForm
    success_url = reverse_lazy('scrapper:list')


def import_movie_view(request):
    template_name = 'data_importer/template_importer.html'
    context = {
        'title': 'Movie Importer',
        'form': FileUploadHistoryForm()
    }

    if request.method == 'POST':
        form = FileUploadHistoryForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.name = 'Movie Importer'
            instance.user = request.user
            instance.save()

            importer = MovieImporter(instance=instance)
            importer.execute()

            context.update({
                'form': form,
                'message': 'File imported successfully.'
            })

        else:
            context.update({'form': form})

    return render(request, template_name, context)
