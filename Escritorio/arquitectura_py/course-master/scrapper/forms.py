from django import forms


from .models import Movie


class MovieForm(forms.ModelForm):

    movie = forms.ModelChoiceField(
        queryset=Movie.objects.all()
    )

    class Meta:
        model = Movie
        fields = ('movie_name', 'movie_year', 'movie_rating')
