from django.db import models


class Movie(models.Model):
    movie_name = models.CharField(max_length=300)
    movie_year = models.IntegerField()
    movie_rating = models.FloatField()
    deleted = models.BooleanField(default=False)

def __str__(self):
    return '{0} {1} {2}' .format(self.movie_name, self.movie_year, self.movie_rating)
# Create your models here.
