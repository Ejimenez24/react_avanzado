from django.contrib import admin

from data_importer.models import FileUploadHistory


@admin.register(FileUploadHistory)
class FileUploadHistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'start_execution', 'finish_execution', 'user', 'state')
    search_fields = ('id', 'name')
    list_filter = ('state', )

# Register your models here.
