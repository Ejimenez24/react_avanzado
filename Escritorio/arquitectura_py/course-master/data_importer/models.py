from django.db import models

from django.contrib.auth.models import User


class FileUploadHistory(models.Model):
    PROCESSING = 'Processing'
    PROCESSED = 'Processed'
    FAILED = 'Failed'

    STATE_CHOICES = (
        (PROCESSING, PROCESSING),
        (PROCESSING, PROCESSING),
        (PROCESSING, PROCESSING),
    )

    name = models.CharField(max_length=250)
    start_execution = models.DateTimeField(auto_now_add=True)
    finish_execution = models.DateTimeField(null=True, blank=True)
    file_uploaded = models.FileField(upload_to='files/')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    errors = models.JSONField(default=dict)
    state = models.CharField(max_length=100, choices=STATE_CHOICES, default=PROCESSING)

    def __str__(self):
        return '[{0}] {1}'.format(self.start_execution, self.name)

# Create your models here.
