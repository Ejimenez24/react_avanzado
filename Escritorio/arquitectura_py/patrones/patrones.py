'''Patron de diseño Singleton'''

class numeros(object):

    _instance = None
    numero = None

    def __new__(cls):
        if numeros._instance is None:
            print("Nueva instacia de la clase numero")
            numeros._instance = object.__new__(cls)

            return numeros._instance

numero1 = numeros()
numero1.numero = "Este mensaje se imprimio con Singleton"
print(numero1)

'''Patron de diseño Decorator'''

def funcion_a(funcion_b):
    def funcion_c(*args, **kwargs):
        print('Antes de la ejecución de la función a decorar')
        result = funcion_b(*args, **kwargs)
        print('Después de la ejecución de la función a decorar')    

        return result

    return funcion_c

@funcion_a
def suma(a, b):
    return a + b

decorador = funcion_a(suma)
decorador(10, 20)
print(funcion_a())


