from pprint import pprint

from bs4 import BeautifulSoup
import requests
import pandas as pd
import csv

url = 'https://www.ecartelera.com/listas/mejores-peliculas/'
page = requests.get(url)
soup = BeautifulSoup(page.content, 'html.parser')
#Peliculas
movie_name = soup.find_all('p', class_='title')
movie_year = soup.find_all('div', class_='year')
movie_rating = soup.find_all('div', class_='nota verde')

movie_names = list()
count = 0
for i in movie_name:
    if count < 90:
        movie_names.append(i.text)
    else:
        break
    count += 1

movie_years = list()
count = 0
for i in movie_year:
    if count < 90:
        movie_years.append(i.text)
    else:
        break
    count += 1

movie_ratings = list()
count = 0
for i in movie_rating:
    if count < 90:
        movie_ratings.append(i.text)
    else:
        break
    count += 1

mov = pd.DataFrame({'Peliculas': movie_names,'Año': movie_years, 'Rating': movie_ratings}, index=list(range(1,91)))

mov.to_csv('Lista Peliculas.csv', index=False)